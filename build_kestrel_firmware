#!/bin/bash
#
# Copyright 2022 Raptor Engineering, LLC
# Licensed under the terms of the GNU LGPLv3+
#
# Ensure that the LibreSoC packages and HDL tools
# are installed prior to running this script,
# as well as the kestrel repos
#
#   ./install-hdl-apt-reqs
#   ./hdl-dev-repos
#   ./hdl-tools-yosys
#   ./ppc64-gdb-gcc
#   ./nextpnr-ecp5
#   ./hdl-kestrel-repos

set -e

export PATH=/usr/local/libtrellis/bin:$PATH
export PATH=/usr/local/nextpnr-ecp5/bin:$PATH

export KESTREL=/home/$SUDO_USER/src/kestrel
echo "Building ppc64el soft floating point library..."
cd $KESTREL/firmware/gcc/libgcc/soft-fp
# Ignore expected errors
set +e
gcc -c -O2 -msoft-float -mno-string -mno-multiple -mno-vsx \
           -mno-altivec -mlittle-endian -mstrict-align \
           -fno-stack-protector -m64 -mabi=elfv2 -mcmodel=small \
           -I../config/rs6000/ -I../../include -I.. *.c &> /dev/null
set -e
ar -crv libsoft-fp.a *.o
cp -Rp libsoft-fp.a ../../../

echo "Building Zephyr..."
cd $KESTREL/firmware
export SOURCE_ROOT_DIR=$(pwd)
cd zephyr-rtos
rm -rf build
mkdir build
cd build
export ZEPHYR_MODULES="${SOURCE_ROOT_DIR}/zephyr-littlefs"
export ZEPHYR_MODULES="${ZEPHYR_MODULES};${SOURCE_ROOT_DIR}/zephyr-mbedtls;"
export ZEPHYR_MODULES="${ZEPHYR_MODULES};${SOURCE_ROOT_DIR}/zephyr-civetweb"
ZEPHYR_BASE=${SOURCE_ROOT_DIR}/zephyr-rtos \
           ZEPHYR_TOOLCHAIN_VARIANT=host \
           cmake -DZEPHYR_MODULES="${ZEPHYR_MODULES}" \
           -DBOARD=litex_kestrel ${SOURCE_ROOT_DIR}/zephyr-firmware
cp -Rp ../../libsoft-fp.a zephyr/
make -j32

echo -n "Zephyr firmware binary now in "
echo    "$KESTREL/firmware/zephyr-rtos/build/zephyr/zephyr.bin"

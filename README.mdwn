# Libre-SOC Reproducible Build Scripts

This repository contains the Libre-SOC "reproducible builds" scripts.
Initially begun as a way to ensure that coriolis2 VLSI ASIC layouts
were reproducible, they've turned into an easy way for developers to
get started.  

All packages are installed within a baseline debian/10 debootstrapped
schroot.  For reproducible builds, you have to pick an OS, and debian/10
is good enough.  Note, obviously, that adding security updates would
completely defeat the purpose and object of being "reproducible", so
it is *strongly recommended* that you do **not** make these chrooted
installs internet-facing.

Prerequisites:

* Strongly recommended that you install a Host OS of Debian.
* Confirmed working but not supported: ubuntu
* Also confirmed working with a caveat on X11: archlinux,
  (which happens to have a port of debootstrap)

Installs (at recent but usually specific - reproducible - versions):

* coriolis2
* yosys
* symbiyosys, z3 solver, yices, sby
* ghdl
* yosys-ghdl
* nextpnr-ecp5
* nextpnr-xilinx
* symbiflow
* nmigen (a Registered Trademark of M-Labs)
* verilator
* icarus verilog
* cocotb
* a ton of FPGA loader tools, dfu-util, openFPGALoader, ecpprog,
  fujprog, and associated udev.d rules files.

Note: we don't use conda.  we don't use docker.

# Why reproducible?

Imagine that you are about to drop USD 16 million on 7nm mask charges.
Two - or more - engineers run the VLSI build scripts, to create the
Multi-Terabyte GDS-II Files.  You have a week before the deadline: it
takes 48 hours to run the VLSI build.  You find that, after 48 hours,
a comparison of all the engineer's GDS-II files show differences.

More than a week's worth of investigation - long after the deadline
has been missed to submit the Masks to the Foundry (which you paid for
up-front and now cannot recover) - you discover that one developer
used Ubuntu and had a PRNG library that created a different Place-and-Route
sequence, another used BSD which had an older library with a bug, and yet
another had installed security updates which changed memory-allocation.

The "perfect" reproducible build system uses absolutely identical hardware,
installs on bare-metal (no Virtual Machines), does a complete and full
OS wipe and automated install, and, after running, if two *Drives* are
compared, they should be bit-level identical.  This is of course a little
extreme (but hey, you're spending USD 16 million on 7nm masks then you're
entitled to a little paranoia) so the closest we can get is:

* (a) pick an OS
* (b) don't add updates and
* (c) pin as many packages with explicit versions and git tags as possible.

It works. don't knock it.

# Installation Notes

During their development, each install script had notes taken which,
if followed carefully, should result in the replication of the script.
These notes can be found here:

* <https://libre-soc.org/HDL_workflow/>
* <https://libre-soc.org/HDL_workflow/coriolis2/>
* <https://libre-soc.org/HDL_workflow/nextpnr/>
* <https://libre-soc.org/HDL_workflow/nextpnr-xilinx/>
* <https://libre-soc.org/HDL_workflow/verilator/>
* <https://libre-soc.org/HDL_workflow/ghdl/>
* <https://libre-soc.org/HDL_workflow/iverilog/>
* <https://libre-soc.org/HDL_workflow/cocotb/>
* <https://libre-soc.org/HDL_workflow/symbiflow/>
* <https://libre-soc.org/HDL_workflow/fpga-boot-loaders-progs/>

# Contact details

Any questions, improvements, bug-fixes or patches, most welcome:
please join Libera.Chat IRC #libre-soc, or join the mailing list:
<https://lists.libre-soc.org/mailman/listinfo/libre-soc-dev>

# Charter

Covered by the Libre-SOC Charter <https://libre-soc.org/charter/discussion/>
we particularly welcome appropriate Dilbert Cartoons illustrating the
antithetical polar opposite of the points that make up the fundamental
existential existence of the Charter. y'ken.
